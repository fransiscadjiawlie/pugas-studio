//
//  listViewController.swift
//  Pugas Studio Calculator
//
//  Created by Fransisca Amelia Wijaya on 09/05/22.
//

import UIKit

struct List {
    let title:String
    let label:String
    let labelDuration:String
    let labelPrice:String
    var stepCount:Int=0

    // Contoh
    var firstRange: Int = 100
    var lastRange: Int = 200
    
    var firstDay: Int = 1
    var lastDay: Int = 2
}

class listViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
   
    

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.searchController = searchController
       
  
        
        tableView.dataSource = self
        tableView.delegate = self
    }
   
    var data: [List] = [
        List(title: "Logo", label: "service 1", labelDuration: "3-7 days", labelPrice: "372.000-965.000", firstRange: 372000, lastRange: 965000, firstDay: 3, lastDay:7),
        List(title: "Business Card", label: "service 2", labelDuration: "1-2 days", labelPrice: "72.000-100.000",firstRange: 72000, lastRange: 100000, firstDay: 1, lastDay:2),
        List(title: "Stationary", label: "service 3", labelDuration: "1-3 days", labelPrice: "96.000-150.000",firstRange: 96000, lastRange: 150000, firstDay: 1, lastDay:3),
        List(title: "Label", label: "service 4", labelDuration: "1-3 days", labelPrice: "96.000-150.000", firstRange: 96000, lastRange: 150000, firstDay: 1, lastDay:3),
        List(title: "Packaging", label: "service 5", labelDuration: "3-4 days", labelPrice: "325.000-600.000",firstRange: 325000, lastRange: 600000, firstDay: 3, lastDay:4),
        List(title: "T-shirt", label: "service 6", labelDuration: "2-3 days", labelPrice: "150.000-499.000",firstRange: 150000, lastRange: 499000, firstDay: 2, lastDay:3),
        
    ]
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt IndexPath: IndexPath)-> UITableViewCell{
        
        let list = data[IndexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: IndexPath) as! CustomTableViewCell
        
        
        cell.label.text = "\(list.stepCount) \(list.title)"
        
        cell.labelDuration.text = "\(list.firstDay)-\(list.lastDay) Days"
        cell.labelPrice.text = "\(NumberFormatterHelper.convertToRupiah(value: list.firstRange) ?? "0")-\(NumberFormatterHelper.convertToRupiah(value: list.lastRange) ?? "0")"
        
        
        cell.stepper.addTarget(self, action: #selector(update(_:)), for:.valueChanged)
        
        cell.stepper.tag=IndexPath.row
        cell.stepper.value = Double(list.stepCount)
        
        cell.labelPrice.accessibilityLabel = "\(list.firstRange)rupiah until\(list.lastRange)rupiah "
        cell.labelDuration.accessibilityLabel = "\(list.firstDay)days until\(list.lastDay)days"
        
        if list.stepCount >= 1 {
            //data[IndexPath.row].firstRange = list.firstRange * list.stepCount
            print(data[IndexPath.row].firstRange)
            cell.labelPrice.text = "\(NumberFormatterHelper.convertToRupiah(value: list.firstRange*list.stepCount) ?? "0")-\(NumberFormatterHelper.convertToRupiah(value: list.lastRange*list.stepCount) ?? "0")"
            cell.labelDuration.text = "\(list.firstDay*list.stepCount)-\(list.lastDay*list.stepCount) Days"
            
            
           
            
       
        }
        
        return cell
    }
    
    @objc func update(_ sender: UIStepper){
        
        //sender.tag=Int(sender.value)
    
        
        data[sender.tag].stepCount = Int(sender.value)
        
        print("Ini stepper dengan tag: \(sender.tag) valuenya \(sender.value)" )
        
        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    @IBAction func estimateButton(_ sender: UIButton) {
        var data = [List]()
        var totalServices: Int = 0
        var totalMinDay: Int = 0
        var totalMaxDay: Int = 0
        var totalMinPrice: Int = 0
        var totalMaxPrice: Int = 0
        
        for value in self.data {
            if value.stepCount >= 1 {
                print (value)
                
                totalServices = totalServices+value.stepCount
                totalMinDay = totalMinDay+(value.firstDay*value.stepCount)
                totalMaxDay = totalMaxDay+(value.lastDay*value.stepCount)
                totalMinPrice = totalMinPrice+(value.firstRange*value.stepCount)
                totalMaxPrice = totalMaxPrice+(value.lastRange*value.stepCount)
                
                data.append(value)
            }
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let addViewController = storyboard.instantiateViewController(withIdentifier: "Add") as? estimationViewController
        if let addViewController = addViewController {
            addViewController.data = data
            
            addViewController.totalServices = totalServices
            addViewController.totalMinDay = totalMinDay
            addViewController.totalMaxDay = totalMaxDay
            addViewController.totalMinPrice = totalMinPrice
            addViewController.totalMaxPrice = totalMaxPrice
            
            addViewController.delegate=self
            
            let navigationController = UINavigationController(rootViewController: addViewController)
            
            self.navigationController?.present(navigationController, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class NumberFormatterHelper {
    static func convertToRupiah(value: Int) -> String? {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter.string(from: value as NSNumber)
    }
}
extension listViewController: ModalControllerDelegate {
    func modalWillDisappear<T>(_ modal: T) {
        // Update List after input / update data
        for (index,_) in self.data.enumerated() {
            self.data[index].stepCount = 0
        }
        tableView.reloadData()
    }
}
