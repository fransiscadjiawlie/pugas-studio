//
//  estimationViewController.swift
//  Pugas Studio Calculator
//
//  Created by Fransisca Amelia Wijaya on 12/05/22.
//

import UIKit


class estimationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    

    var data = [List]()
    var totalServices: Int = 0
    var totalMinDay: Int = 0
    var totalMaxDay: Int = 0
    var totalMinPrice: Int = 0
    var totalMaxPrice: Int = 0
   
    @objc func editData (){
       dismiss(animated: true)
    }
    @objc func resetData (){
        delegate?.modalWillDisappear(self)
        dismiss(animated: true)
    }
    var delegate:ModalControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalService: UILabel!
    @IBOutlet weak var totalDay: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    override func viewWillDisappear(_ animated: Bool) {
                super.viewWillDisappear(animated)
    }
    override func viewDidLoad() {
    
        navigationItem.leftBarButtonItem=UIBarButtonItem (title: "edit", style: .plain, target: self, action: #selector(editData))
        
        navigationItem.rightBarButtonItem=UIBarButtonItem (title: "reset", style: .plain, target: self, action: #selector(resetData))
        
    
        
        title = "Estimation"
        
        navigationController?.navigationBar.backgroundColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
        
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        totalService.text = "\(totalServices) \(totalServices>0 ? "Designs":"Design")"
        totalDay.text =
        "\(totalMinDay)-\(totalMaxDay) \(totalMinDay>1 ? "Days":"Day")"
        totalPrice.text = "\(NumberFormatterHelper.convertToRupiah(value: totalMinPrice) ?? "0")-\(NumberFormatterHelper.convertToRupiah(value: totalMaxPrice) ?? "0")"
  
        
     
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt IndexPath: IndexPath)-> UITableViewCell{
        
        let list = data[IndexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: IndexPath) as! estimationTableViewCell
        
        
        cell.labelText.text = "\(list.stepCount) \(list.title)"
        
        cell.labelDuration.text = "\(list.firstDay*list.stepCount)-\(list.lastDay*list.stepCount) Days"
        cell.labelPrice.text = "\(NumberFormatterHelper.convertToRupiah(value: list.firstRange*list.stepCount) ?? "0")-\(NumberFormatterHelper.convertToRupiah(value: list.lastRange*list.stepCount) ?? "0")"
        
        cell.labelPrice.accessibilityLabel = "\(list.firstRange)rupiah until\(list.lastRange)rupiah "
        cell.labelDuration.accessibilityLabel = "\(list.firstDay)days until\(list.lastDay)days"
        
      
   
      
        
            
    
        
        return cell
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
protocol ModalControllerDelegate {
    func modalWillDisappear<T>(_ modal: T)
}
