//
//  CustomTableViewCell.swift
//  Pugas Studio Calculator
//
//  Created by Fransisca Amelia Wijaya on 09/05/22.
//

import UIKit

class CustomTableViewCell: UITableViewCell {


        @IBOutlet weak var label:UILabel!
  

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
