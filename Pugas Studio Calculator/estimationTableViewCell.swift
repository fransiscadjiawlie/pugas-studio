//
//  estimationTableViewCell.swift
//  Pugas Studio Calculator
//
//  Created by Fransisca Amelia Wijaya on 12/05/22.
//

import UIKit

class estimationTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
